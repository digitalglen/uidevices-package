import XCTest

import UIDevicesTests

var tests = [XCTestCaseEntry]()
tests += UIDevicesTests.allTests()
XCTMain(tests)
