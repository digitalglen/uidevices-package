import UIKit

public extension UIDevice {
    enum Platform : Int {
        case iPod = 0
        case iPhone = 1
        case iPad = 2
        case OSX = 3
        case tvOS = 4
        case watchOS = 5
        case simulator = 6
        case unknown = 7
    }
    enum Model : Int {
        case iPodTouch5 = 0
        case iPodTouch6 = 1
        case iPhone4 = 2
        case iPhone4s = 3
        case iPhone5 = 4
        case iPhone5c = 5
        case iPhone5s = 6
        case iPhone6 = 7
        case iPhone6Plus = 8
        case iPhone6s = 9
        case iPhone6sPlus = 10
        case iPhoneSE = 11
        case iPad2 = 12
        case iPad3 = 13
        case iPad4 = 14
        case iPadAir = 15
        case iPadAir2 = 16
        case iPadMini = 17
        case iPadMini2 = 18
        case iPadMini3 = 19
        case iPadMini4 = 20
        case iPadPro97 = 21
        case iPadPro129 = 22
        case AppleTV = 23
        case Simulator = 24
        case Unknown = 25
        case iPhone7 = 26
        case iPhone7Plus = 27
        case iPadPro105 = 28
        case iPadPro1292 = 29
        case iPhone8 = 30
        case iPhone8Plus = 31
        case iPhoneX = 32
        case iPhoneXS = 33
        case iPhoneXSMax = 34
        case iPhoneXR = 35
        case iPhone11 = 36
        case iPhone11Pro = 37
        case iPhone11ProMax = 38
        case iPhoneSE2 = 39
        case iPhone12Mini = 40
        case iPhone12 = 41
        case iPhone12Pro = 42
        case iPhone12ProMax = 43
        case iPhone13Mini = 44
        case iPhone13 = 45
        case iPhone13Pro = 46
        case iPhone13ProMax = 47

        case iPad5 = 48
        case iPad6 = 49
        case iPad7 = 50
        case iPad8 = 51
        case iPad9 = 52
        case iPadAir3 = 53
        case iPadAir4 = 54
        case iPadMini5 = 55
        case iPadMini6 = 56
        case iPadPro11 = 57
        case iPadPro112 = 58
        case iPadPro113 = 59
        case iPadPro1293 = 60
        case iPadPro1294 = 61
        case iPadPro1295 = 62
        case AppleTV4K = 63
    }
    enum ScreenType : Int {
        case iPhone3_5Inch = 0
        case iPhone4Inch = 1     // includes 5th & 6h gen iPod Touch
        case iPhone4_7Inch = 2
        case iPhone5_5Inch = 3
        case iPad = 4
        case iPad_12_9Inch = 5
        case TV = 6
        case Unknown = 7
        case iPad_10_5Inch = 8
        case iPhone5_8Inch = 9
    }
    
    private var identifier : String {
        var systemInfo = utsname()
        uname(&systemInfo)

        // OPTIMIZATION START
        if let cached_identifier = UserDefaults.standard.string(forKey: "device_id") {
            return cached_identifier;
        }
        // OPTIMIZATION END
        
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        // OPTIMIZATION START
        UserDefaults.standard.set(identifier, forKey:"device_id")
        // OPTIMIZATION END
        
        return identifier
    }
    
    //MARK: - Platform
    
    var platform : Platform {
        return platformForModel(model)
    }
    
    private func platformForModel(_ model : Model) -> Platform {
        switch model {
        case .iPodTouch5:    return .iPod
        case .iPodTouch6:    return .iPod
        case .iPhone4:       return .iPhone
        case .iPhone4s:      return .iPhone
        case .iPhone5:       return .iPhone
        case .iPhone5c:      return .iPhone
        case .iPhone5s:      return .iPhone
        case .iPhone6:       return .iPhone
        case .iPhone6Plus:   return .iPhone
        case .iPhone6s:      return .iPhone
        case .iPhone6sPlus:  return .iPhone
        case .iPhone7:       return .iPhone
        case .iPhone7Plus:   return .iPhone
        case .iPhone8:       return .iPhone
        case .iPhone8Plus:   return .iPhone
        case .iPhoneSE:      return .iPhone
        case .iPhoneX:       return .iPhone
        case .iPad2:         return .iPad
        case .iPad3:         return .iPad
        case .iPad4:         return .iPad
        case .iPadAir:       return .iPad
        case .iPadAir2:      return .iPad
        case .iPadMini:      return .iPad
        case .iPadMini2:     return .iPad
        case .iPadMini3:     return .iPad
        case .iPadMini4:     return .iPad
        case .iPadPro129:    return .iPad
        case .iPadPro97:     return .iPad
        case .iPadPro105:    return .iPad
        case .iPadPro1292:   return .iPad
        case .AppleTV:       return .tvOS
        case .Simulator:     return .simulator
        case .Unknown:       return .unknown
        case .iPhoneXS:      return .iPhone
        case .iPhoneXSMax:   return .iPhone
        case .iPhoneXR:      return .iPhone
        case .iPhone11:      return .iPhone
        case .iPhone11Pro:   return .iPhone
        case .iPhone11ProMax: return .iPhone
        case .iPhoneSE2:     return .iPhone
        case .iPhone12Mini:  return .iPhone
        case .iPhone12:      return .iPhone
        case .iPhone12Pro:   return .iPhone
        case .iPhone12ProMax: return .iPhone
        case .iPhone13Mini:  return .iPhone
        case .iPhone13:      return .iPhone
        case .iPhone13Pro:   return .iPhone
        case .iPhone13ProMax: return .iPhone

        case .iPad5:         return .iPad
        case .iPad6:         return .iPad
        case .iPad7:         return .iPad
        case .iPad8:         return .iPad
        case .iPad9:         return .iPad
        case .iPadAir3:      return .iPad
        case .iPadAir4:      return .iPad
        case .iPadMini5:     return .iPad
        case .iPadMini6:     return .iPad
        case .iPadPro11:     return .iPad
        case .iPadPro112:    return .iPad
        case .iPadPro113:    return .iPad
        case .iPadPro1293:   return .iPad
        case .iPadPro1294:   return .iPad
        case .iPadPro1295:   return .iPad
        case .AppleTV4K:     return .tvOS
        }
    }
    
    var platformName : String {
        return platformName(forPlatform: platform)
    }
    func platformName(forPlatform: Platform) -> String {
        switch (forPlatform) {
        case .iPod:       return "iPod"
        case .iPhone:     return "iPhone"
        case .OSX:        return "OS X"
        case .iPad:       return "iPad"
        case .tvOS:       return "iPhone 4s"
        case .simulator:  return "Simulator"
        case .watchOS:    return "watchOS"
        case .unknown:    return "Unknown"
        }
    }
    
    
    //MARK: - Model
    
    var model : Model {
        return modelForIdentifier(identifier)
    }
    
    private func modelForIdentifier(_ identifier : String) -> Model {
        let lookup : [String : Model] = [
            "iPod5,1":   .iPodTouch5,
            "iPod7,1":   .iPodTouch6,
            "iPhone3,1": .iPhone4,
            "iPhone3,2": .iPhone4,
            "iPhone3,3": .iPhone4,
            "iPhone4,1": .iPhone4s,
            "iPhone5,1": .iPhone5,
            "iPhone5,2": .iPhone5,
            "iPhone5,3": .iPhone5c,
            "iPhone5,4": .iPhone5c,
            "iPhone6,1": .iPhone5s,
            "iPhone6,2": .iPhone5s,
            "iPhone7,2": .iPhone6,
            "iPhone7,1": .iPhone6Plus,
            "iPhone8,1": .iPhone6s,
            "iPhone8,2": .iPhone6sPlus,
            "iPhone8,4": .iPhoneSE,
            "iPhone9,1": .iPhone7,
            "iPhone9,3": .iPhone7,
            "iPhone9,2": .iPhone7Plus,
            "iPhone9,4": .iPhone7Plus,
            "iPhone10,1": .iPhone8,
            "iPhone10,4": .iPhone8,
            "iPhone10,2": .iPhone8Plus,
            "iPhone10,5": .iPhone8Plus,
            "iPhone10,3": .iPhone8Plus,
            "iPhone10,6": .iPhoneX,
            "iPhone11,2": .iPhoneXS,
            "iPhone11,4": .iPhoneXSMax,
            "iPhone11,8": .iPhoneXR,
            "iPhone12,1": .iPhone11,
            "iPhone12,3": .iPhone11Pro,
            "iPhone12,5": .iPhone11ProMax,
            "iPhone12,8": .iPhoneSE2,
            "iPhone13,1": .iPhone12Mini,
            "iPhone13,2": .iPhone12,
            "iPhone13,3": .iPhone12Pro,
            "iPhone13,4": .iPhone12ProMax,
            "iPhone14,4": .iPhone13Mini,
            "iPhone14,5": .iPhone13,
            "iPhone14,2": .iPhone13Pro,
            "iPhone14,3": .iPhone13ProMax,
            "iPad2,1":    .iPad2,
            "iPad2,2":    .iPad2,
            "iPad2,3":    .iPad2,
            "iPad2,4":    .iPad2,
            "iPad3,1":    .iPad3,
            "iPad3,2":    .iPad3,
            "iPad3,3":    .iPad3,
            "iPad3,4":    .iPad4,
            "iPad3,5":    .iPad4,
            "iPad3,6":    .iPad4,
            "iPad4,1":    .iPadAir,
            "iPad4,2":    .iPadAir,
            "iPad4,3":    .iPadAir,
            "iPad5,3":    .iPadAir2,
            "iPad5,4":    .iPadAir2,
            "iPad2,5":    .iPadMini,
            "iPad2,6":    .iPadMini,
            "iPad2,7":    .iPadMini,
            "iPad4,4":    .iPadMini2,
            "iPad4,5":    .iPadMini2,
            "iPad4,6":    .iPadMini2,
            "iPad4,7":    .iPadMini3,
            "iPad4,8":    .iPadMini3,
            "iPad4,9":    .iPadMini3,
            "iPad5,1":    .iPadMini4,
            "iPad5,2":    .iPadMini4,
            "iPad6,7":    .iPadPro129,
            "iPad6,8":    .iPadPro129,
            "iPad6,3":    .iPadPro97,
            "iPad6,4":    .iPadPro97,
            "iPad7,3":    .iPadPro105,
            "iPad7,4":    .iPadPro105,
            "iPad7,1":    .iPadPro1292,
            "iPad7,2":    .iPadPro1292,
        
            "AppleTV5,3": .AppleTV,
            "i386":       .Simulator,
            "x86_64":     .Simulator,
            
            "iPad6,11": .iPad5,
            "iPad6,12": .iPad5,
            "iPad7,5":   .iPad6,
            "iPad7,6":   .iPad6,
            "iPad7,11":   .iPad7,
            "iPad7,12":   .iPad7,
            "iPad11,6":   .iPad8,
            "iPad11,7":   .iPad8,
            "iPad12,1":   .iPad9,
            "iPad12,2":   .iPad9,
            "iPad11,3":   .iPadAir3,
            "iPad11,4":   .iPadAir3,
            "iPad13,1":   .iPadAir4,
            "iPad13,2":   .iPadAir4,
            "iPad11,1":   .iPadMini5,
            "iPad11,2":   .iPadMini5,
            "iPad14,1":   .iPadMini6,
            "iPad14,2":   .iPadMini6,
            "iPad8,1":    .iPadPro11,
            "iPad8,2":    .iPadPro11,
            "iPad8,3":    .iPadPro11,
            "iPad8,4":    .iPadPro11,
            "iPad8,9":    .iPadPro112,
            "iPad8,10":   .iPadPro112,
            "iPad13,4":   .iPadPro113,
            "iPad13,5":   .iPadPro113,
            "iPad13,6":   .iPadPro113,
            "iPad13,7":   .iPadPro113,
            "iPad8,5":    .iPadPro1293,
            "iPad8,6":    .iPadPro1293,
            "iPad8,7":    .iPadPro1293,
            "iPad8,8":    .iPadPro1293,
            "iPad8,11":   .iPadPro1294,
            "iPad8,12":   .iPadPro1294,
            "iPad13,8":   .iPadPro1295,
            "iPad13,9":   .iPadPro1295,
            "iPad13,10":  .iPadPro1295,
            "iPad13,11":  .iPadPro1295,
            "AppleTV6,2": .AppleTV4K,
            ]
        for key in lookup.keys {
            if key == identifier {
                return lookup[key]!
            }
        }
        return .Unknown
    }
    
    
    var modelName : String {
        return modelName(forModel: model)
    }
    
    private func modelName(forModel: Model) -> String {
        switch (forModel) {
        case .iPodTouch5:    return "iPod Touch 5"
        case .iPodTouch6:    return "iPod Touch 6"
        case .iPhone4:       return "iPhone 4"
        case .iPhone4s:      return "iPhone 4s"
        case .iPhone5:       return "iPhone 5"
        case .iPhone5c:      return "iPhone 5c"
        case .iPhone5s:      return "iPhone 5s"
        case .iPhone6:       return "iPhone 6"
        case .iPhone6Plus:   return "iPhone 6 Plus"
        case .iPhone6s:      return "iPhone 6s"
        case .iPhone6sPlus:  return "iPhone 6s Plus"
        case .iPhone7:       return "iPhone 7"
        case .iPhone7Plus:   return "iPhone 7 Plus"
        case .iPhone8:       return "iPhone 8"
        case .iPhone8Plus:   return "iPhone 8 Plus"
        case .iPhoneSE:      return "iPhone SE"
        case .iPhoneX:       return "iPhone X"
        case .iPhoneXS:      return "iPhone XS"
        case .iPhoneXSMax:   return "iPhone XS Max"
        case .iPhoneXR:      return "iPhone XR"
        case .iPhone11:      return "iPhone 11"
        case .iPhone11Pro:   return "iPhone 11 Pro"
        case .iPhone11ProMax: return "iPhone 11 Pro Max"
        case .iPhoneSE2:     return "iPhone SE (2nd Gen)"
        case .iPhone12Mini:  return "iPhone 12 mini"
        case .iPhone12:      return "iPhone 12"
        case .iPhone12Pro:   return "iPhone 12 Pro"
        case .iPhone12ProMax: return "iPhone 12 Pro Max"
        case .iPhone13Mini:  return "iPhone 13 mini"
        case .iPhone13:      return "iPhone 13"
        case .iPhone13Pro:   return "iPhone 13 Pro"
        case .iPhone13ProMax: return "iPhone 13 Pro Max"
        case .iPad2:         return "iPad 2"
        case .iPad3:         return "iPad 3"
        case .iPad4:         return "iPad 4"
        case .iPadAir:       return "iPad Air"
        case .iPadAir2:      return "iPad Air 2"
        case .iPadMini:      return "iPad Mini"
        case .iPadMini2:     return "iPad Mini 2"
        case .iPadMini3:     return "iPad Mini 3"
        case .iPadMini4:     return "iPad Mini 4"
        case .iPadPro129:    return "iPad Pro 12.9 inch"
        case .iPadPro97:     return "iPad Pro 9.7 inch"
        case .iPadPro105:    return "iPad Pro 10.5 inch"
        case .iPadPro1292:   return "iPad Pro 12.9 inch, 2nd Gen"
        case .AppleTV:       return "Apple TV"
        case .Simulator:     return "Simulator"
        case .Unknown:       return identifier


        case .iPad5:         return "iPad 5"
        case .iPad6:         return "iPad 6"
        case .iPad7:         return "iPad 7"
        case .iPad8:         return "iPad 8"
        case .iPad9:         return "iPad 9"
        case .iPadAir3:      return "iPad Air 3"
        case .iPadAir4:      return "iPad Air 4"
        case .iPadMini5:     return "iPad Mini 5"
        case .iPadMini6:     return "iPad Mini 6"
        case .iPadPro11:     return "iPad Pro 11 inch"
        case .iPadPro112:    return "iPad Pro 11 inch, 2nd Gen"
        case .iPadPro113:    return "iPad Pro 11 inch, 3nd Gen"
        case .iPadPro1293:   return "iPad Pro 12.9 inch, 3nd Gen"
        case .iPadPro1294:   return "iPad Pro 12.9 inch, 4nd Gen"
        case .iPadPro1295:   return "iPad Pro 12.9 inch, 5nd Gen"
        case .AppleTV4K:     return "Apple TV 4K"
        }
    }
    
    //MARK: - ScreenType
    
    var screenType : ScreenType {
        return screenType(forModel: model)
    }
    
    private func screenType(forModel: Model) -> ScreenType {
        switch (forModel) {
        case .iPodTouch5:    return .iPhone4Inch
        case .iPodTouch6:    return .iPhone4Inch
        case .iPhone4:       return .iPhone3_5Inch
        case .iPhone4s:      return .iPhone3_5Inch
        case .iPhone5:       return .iPhone4Inch
        case .iPhone5c:      return .iPhone4Inch
        case .iPhone5s:      return .iPhone4Inch
        case .iPhone6:       return compensateForZoom(.iPhone4_7Inch)
        case .iPhone6Plus:   return compensateForZoom(.iPhone5_5Inch)
        case .iPhone6s:      return compensateForZoom(.iPhone4_7Inch)
        case .iPhone6sPlus:  return compensateForZoom(.iPhone5_5Inch)
        case .iPhone7:       return compensateForZoom(.iPhone4_7Inch)
        case .iPhone7Plus:   return compensateForZoom(.iPhone5_5Inch)
        case .iPhone8:       return compensateForZoom(.iPhone4_7Inch)
        case .iPhone8Plus:   return compensateForZoom(.iPhone5_5Inch)
        case .iPhoneX:       return .iPhone5_8Inch
        case .iPad2:         return .iPad
        case .iPad3:         return .iPad
        case .iPad4:         return .iPad
        case .iPadAir:       return .iPad
        case .iPadAir2:      return .iPad
        case .iPadMini:      return .iPad
        case .iPadMini2:     return .iPad
        case .iPadMini3:     return .iPad
        case .iPadMini4:     return .iPad
        case .iPadPro129:    return .iPad_12_9Inch
        case .iPadPro97:     return .iPad
        case .iPadPro105:    return .iPad
        case .iPadPro1292:   return .iPad_12_9Inch
        case .AppleTV:       return .TV
        case .Simulator:     return guessScreenTypeBasedOnSize()
        case .Unknown:       return guessScreenTypeBasedOnSize()
        default:             return guessScreenTypeBasedOnSize()
        }
    }
    
    private static var screenWidth : Int {return Int(UIScreen.main.bounds.size.width)}
    private static var screenHeight : Int {return Int(UIScreen.main.bounds.size.height)}
    private static var maxScreenDimension : Int {return max(screenWidth, screenHeight)}
    private static var minScreenDimension : Int {return min(screenWidth, screenHeight)}
    
    private func compensateForZoom(_ originalScreenType : ScreenType) -> ScreenType {
        if (originalScreenType == .iPhone4_7Inch) {
            if UIDevice.maxScreenDimension == 568 {
                return .iPhone4Inch
            } else {
                return .iPhone4_7Inch
            }
        } else if originalScreenType == .iPhone5_5Inch {
            if UIDevice.maxScreenDimension == 667 {
                return .iPhone4_7Inch
            } else {
                return .iPhone5_5Inch
            }
        }
        return originalScreenType
    }
    
    private func guessScreenTypeBasedOnSize() -> ScreenType {
        switch UIDevice.maxScreenDimension {
        case 480:       return .iPhone3_5Inch
        case 568:       return .iPhone4Inch
        case 667:       return .iPhone4_7Inch
        case 736:       return .iPhone5_5Inch
        case 1024:      return .iPad
        case 1366:      return .iPad_12_9Inch
        case 1920:      return .TV
        default:
            switch UIDevice.current.userInterfaceIdiom {
            case .pad:   return .iPad
            case .phone: return .iPhone4_7Inch
            case .tv:    return .TV
            default:     return .Unknown
            }
        }
    }
    
    
    var screenTypeName : String {
        return screenTypeName(screenType)
    }
    private func screenTypeName(_ screenType : ScreenType) -> String {
        switch screenType {
        case .iPhone3_5Inch:  return "3.5in"
        case .iPhone4Inch:    return "4.0in"
        case .iPhone4_7Inch:  return "4.7in"
        case .iPhone5_5Inch:  return "5.5in"
        case .iPhone5_8Inch:  return "5.8in"
        case .iPad:           return "10.0in"
        case .iPad_10_5Inch:  return "10.5in"
        case .iPad_12_9Inch:  return "12.9in"
        case .TV:             return "1080p"
        case .Unknown:        return "Unknown"
        }
    }
    
    
    var isiPhone : Bool {
        switch screenType {
        case .iPhone4Inch:    return true
        case .iPhone4_7Inch:  return true
        case .iPhone5_5Inch:  return true
        case .iPhone5_8Inch:  return true
        default: return false
        }
    }
    
}
